package com.teqpace

import io.ktor.http.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlin.test.*
import io.ktor.server.testing.*
import com.teqpace.plugins.*

class ApplicationTest {
    @Test
    fun testRoot() = testApplication {
        application {
            configureSerialization()
            configureRouting()
        }

        client.get("/").apply {
            assertEquals(HttpStatusCode.OK, status)
            assertEquals("Hello World!", bodyAsText())
        }

        client.get("/users").apply {
            assertEquals(HttpStatusCode.OK, status)
        }

        client.get("/users/1").apply {
            assertEquals(HttpStatusCode.OK, status)
        }
    }
}