package com.teqpace.plugins

/**
 * @author Canaan Etai
 * Date 31/12/2022
 * Time 11:04 am
 */

data class CreateUserRequest(
    val name: String,
    val email: String
)

data class UpdateUserRequest(
    val id: Int,
    val name: String,
    val email: String
)


data class IDOnly(
    val id: Int
)