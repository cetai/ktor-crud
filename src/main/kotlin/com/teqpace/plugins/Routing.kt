package com.teqpace.plugins

import com.teqpace.service.UserService
import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*

fun Application.configureRouting() {
    routing {
        get("/") {
            call.respondText("Hello World!")
        }

        route("/users") {
            get {
                val users: Map<String, Any> = UserService().listUsers()

                call.respond(users)
            }
            post {
                val payload = call.receive<CreateUserRequest>()

                val response: Map<String, Any> = UserService().createUser(payload)

                call.respond(response)
            }

            put {
                val payload = call.receive<UpdateUserRequest>()

                val response: Map<String, Any> = UserService().updateUser(payload)

                call.respond(response)
            }

            delete {
                val payload = call.receive<IDOnly>()
                val users: Map<String, Any> = UserService().deleteUser(payload.id)

                call.respond(users)
            }

            get("{id}") {
                val id = call.parameters.get("id")
                val users: Map<String, Any> = UserService().fetchById(id!!.toInt())

                call.respond(users)
            }

        }
    }
}
