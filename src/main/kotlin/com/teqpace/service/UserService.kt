package com.teqpace.service

import com.teqpace.db.UserDB
import com.teqpace.model.User
import com.teqpace.plugins.CreateUserRequest
import com.teqpace.plugins.UpdateUserRequest

/**
 * @author Canaan Etai
 * Date 31/12/2022
 * Time 9:40 am
 */
class UserService {

    fun listUsers(): Map<String, Any>
    {
        val users: MutableList<User> = UserDB().all()

        val response = mutableMapOf("success" to true, "users" to users)

        return response
    }

    fun fetchById(id: Int): Map<String, Any>
    {
        return UserDB().findById(id)
    }

    fun deleteUser(id: Int): Map<String, Any>
    {
        return UserDB().deleteUser(id)
    }

    fun createUser(payload: CreateUserRequest): MutableMap<String, Any>
    {
        return UserDB().createUser(payload)
    }

    fun updateUser(payload: UpdateUserRequest): MutableMap<String, Any>
    {
        val user = User(payload.name, payload.email, payload.id)

        return UserDB().updateUser(user)
    }
}