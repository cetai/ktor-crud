package com.teqpace.db

import com.google.gson.Gson
import com.teqpace.model.User
import com.teqpace.plugins.CreateUserRequest
import io.ktor.utils.io.jvm.javaio.*
import java.io.File

/**
 * @author Canaan Etai
 * Date 31/12/2022
 * Time 10:27 am
 */
class UserDB {

    val result = mutableMapOf<String, Any>("success" to true, "message" to "Account created successfully!")

    /**
     * Fetch all users.
     */
    fun all() : MutableList<User> {
        return getData()
    }

    /**
     * Create a new user.
     */
    fun createUser( payload: CreateUserRequest ) : MutableMap<String, Any> {

        val user = User(payload.name, payload.email, 0)

        val data = getData()

        if ( data.isEmpty() ) {
            user.id = 1
            data.add(user)
        }
        else {
            // Check if we already have this user.
            if ( userExist(user.email, data) ) {
                result["success"] = false
                result["message"] = "A user with this email already exist."
            }

            else {
                data.sortBy { it.id }

                // increment user id
                user.id = data.last().id + 1
                data.add(user)
            }
        }

        updataDB(data)

        if ( result["success"] == true ) {
            result["user"] = user
        }

        return result
    }

    /**
     * Update a user.
     */
    fun updateUser( user: User ) : MutableMap<String, Any>
    {
        val users = getData()
        var userData: User? = null

        println("users is $users")

        users.forEach { item ->
            if (item.id == user.id) {
                userData = item
            }
        }

        if ( userData == null ) {
            result["success"] = false
            result["message"] = "We could not find a user with the provided ID"
        }

        // update the user's record and return the updated data
        // We only want to update the name.
        else {
            users.remove(userData)

            userData!!.name = user.name
            users.add(userData!!)

            updataDB(users);

            result["success"] = true
            result["message"] = "Account updated successfully"
            result["user"] = userData!!
        }

        return result
    }

    /**
     * Fetch a user by its ID.
     */
    fun findById( id: Int ) : MutableMap<String, Any>
    {
        val users = getData()
        var userData: User? = null

        users.forEach { item ->
            if ( item.id == id ) {
                userData = item
            }
        }

        if ( userData == null ) {
            result["success"] = false
            result["message"] = "We could not find a user with the provided ID"
        }

        // update the user's record and return the updated data
        // We only want to update the name.
        else {
            result["success"] = true
            result["user"] = userData!!
        }

        return result
    }


    /**
     * Fetch a user by its ID.
     */
    fun deleteUser( id: Int ) : MutableMap<String, Any>
    {
        val users = getData()
        var userData: User? = null

        users.forEach { item ->
            if ( item.id == id ) {
                userData = item
            }
        }

        if ( userData == null ) {
            result["success"] = false
            result["message"] = "We could not find a user with the provided ID"
        }

        // update the user's record and return the updated data
        // We only want to update the name.
        else {
            result["success"] = true
            result["message"] = "Account deleted successfully"

            // update db
            users.remove(userData)
            updataDB(users)
        }

        return result
    }


    private fun getData() : MutableList<User> {
        val file = File(javaClass.classLoader.getResource("db.json")!!.toURI())
        val content = file.readText()

        return Gson().fromJson(content, Array<User>::class.java).toMutableList()
    }


    private fun updataDB( data: List<User> ) {
        val users = Gson().toJson(data)

        val file = File(javaClass.classLoader.getResource("db.json")!!.toURI())

        file.writeText(users)
    }


    private fun userExist( email: String,  data: MutableList<User> ): Boolean
    {
        val exist = false

        data.forEach { item ->
            if (item.email == email) {
                return true
            }
        }

        return exist
    }

}