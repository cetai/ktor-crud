package com.teqpace.model

/**
 * @author Canaan Etai
 * Date 31/12/2022
 * Time 9:28 am
 */
data class User(
    var name: String,
    var email: String,
    var id: Int
)
